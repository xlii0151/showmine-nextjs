import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDown } from '@fortawesome/free-solid-svg-icons';
import Lang from "../lang/Lang"
class Hidden extends React.Component {
    constructor () {
      super()
      this.state = {
        isHidden: true,
        // language: ''
      }
    }
    toggleHidden () {
        
      this.setState({
        isHidden: !this.state.isHidden
      })
    }

    render () {
      const fadeorNot=this.state.isHidden ? 'textFade' : '';
     
      return (
        
        <div style={{margin:'0px 5px'}} >
          
          <h3>🔥🔥🔥🔥🔥🔥 果中玛瑙—杨梅赛荔枝 澳洲本地出品</h3><br />
          
        【果中玛瑙—杨梅赛荔枝】澳大利亚🇦🇺果园，新鲜杨梅首发开团！数量有限，售完为止！<br /><br />
        <div className={fadeorNot}>澳洲独家果园杨梅，果园精心栽培，新鲜杨梅，新鲜杨梅，新鲜杨梅，当天采摘当天农场拉回来散货。一年一季，一季只有短短的不到一个月，一定不能错过！</div>
          {this.state.isHidden&&<button onClick={this.toggleHidden.bind(this)} style={styles.buttons}>
            {Lang.translate("BUTTON.LOADING")}
            <div><FontAwesomeIcon icon={faArrowDown} size="1x"/></div>
          </button>}
          {!this.state.isHidden && <Child />}
        </div>
      )
    }
  }
  
  const Child = () => (
  <div>
       <img src={"cherry1.jpg"} style={styles.image}/>
       新鲜杨梅怎么吃都好吃，酸甜可口，炎炎夏日特别解渴，吃起来特别过瘾，吃不到垂涎四尺，放在冰箱里夏日做冰食解暑吃。<br /><br />
       因为产季短暂，未免遗憾也可以多买一些自制储备起来！
       <img src={"cherry4.jpg"} style={styles.image}/>
       <img src={"cherry3.jpg"} style={styles.image}/>
       【杨梅果汁🍹】将去核的杨梅放入果汁机榨汁。榨好的果汁放入冰箱冷藏5小时。倒入杯中，还可以加入冰块或者蜂蜜哦~<br /><br />
       【制作美味零食杨梅干】自己在家做的杨梅干无添加剂，无防腐剂，可以放心吃！把杨梅倒进缸里，再加入食盐，腌个五六天捞出，沥去盐液置于阳光下晒干，再经过漂洗、糖渍、晒制等程序，甜滋滋的杨梅干就可以入口了。
        <img src={"cherry6.jpg"} style={styles.image}/>
        【可以治疗拉肚子的杨梅酒】制作杨梅酒也很简单，把放入玻璃瓶中，撒上冰糖，倒入白酒，将瓶子密封好，置于阴凉处一月，即可饮用。时间越久，口感层次越丰富。<br /><br />
        【消暑杨梅沙冰】糖和水混合小火煮至糖化，晾凉別用。杨梅洗净，去核，放搅拌机中搅打成汁。倒入晾涼的糖水混合均匀。放冰箱冷冻1小时然后隔半小时搅拌再冷冻。<br /><br />
        澳洲杨梅季十分短暂，仅一个月，去年一直到产季结束，绝大部分人都没轮到吃上，也是澳洲独家供不应求的水果之一，一年一度的杨梅要珍惜哦！拼手速来啦！
        <img src={"cherry4.jpg"} style={styles.image}/>
        <img src={"cherry5.jpg"} style={styles.image}/>
        <img src={"cherry1.jpg"} style={styles.image}/>
  </div>
  )
// var thewidth=window.innerWidth
const styles={
    image:{ 
        width: "100%",
        margin: 'auto',
        marginTop:"10px",
        display:'flex',
        textAlign:'center',
        justifyContent:'center', 
    },
    buttons:{
        borderRadius: '8px',
        width: '100%',
        height:'60px',
        fontSize: '16px',
        backgroundColor:'#2787d6',
        // display:'flex',
        textAlign:'center',
        justifyContent:'center',
        color:'white',
        margin: 'auto',
        transform:'translateY(10px)',
      
    }
}

export default Hidden;