import React, { Component } from 'react';
import Groupbuy from './Groupbuy';
import Lang from "../lang/Lang";
class CommentForm extends Component{
    constructor(props){
        super(props);
        this.state = {
        active:false, 
        paymentSelection: '团购详情', 
      }
      
    //   this.toggleBuy=this.toggleBuy.bind(this);
    //   this.toggleComment=this.toggleComment.bind(this);
      
    }
    toggleContent = (event) => {

        event.preventDefault();
        this.setState({
          paymentSelection: event.target.value,

        })
      }

    switchContent = (value) => {
        switch (value) {
          case '产品点评':
            return <div>
              <Groupbuy resetFeedbackForm={this.props.resetFeedbackForm}/>
              <center>{Lang.translate("COMMENT.NOCOMMENT")}</center>
            </div>;
          case '问答':
            return <div>
              <p>问答 Content Here</p>
            </div>;
          case '团购详情':
            return <div>
              <p>团购详情</p>
            </div>;
          default:
            return null;
        }
      }
      render () {
        const { paymentSelection } = this.state;
        
        return (
          <div className='DetailContainer'>
            <div style={{margin:'10px 0px'}}>
                <div style={{float:'left'}}>
                    <button style={{width:'110px',padding:'15px',border:'none',cursor:'pointer',backgroundColor:'white',fontSize:'15px'}} onClick={this.toggleContent} value="团购详情" className={paymentSelection ==='团购详情' ? 'active' : null}>
                        {Lang.translate("COMMENT.GROUPBUY")}
                    </button>                 
                </div>
                <div style={{float:'left'}}>
                    <button style={{width:'100px',padding:'15px',border:'none',cursor:'pointer',backgroundColor:'white',fontSize:'15px'}} onClick={this.toggleContent} value="问答" className={paymentSelection ==='问答' ? 'active' : null}>        
                        {Lang.translate("COMMENT.Q&A")}
                    </button>
                </div>
                <div style={{float:'left'}}>    
                    <button style={{width:'110px',padding:'15px 0px',cursor:'pointer',border:'none',backgroundColor:'white',fontSize:'15px'}}  onClick={this.toggleContent} value="产品点评" className={paymentSelection ==='产品点评' ? 'active' : null}>               
                        {Lang.translate("COMMENT.PRODUCTRATE")}         
                    </button>
                </div>
            </div>
                <hr style={{border: '0', borderTop: '2px solid #ffbc21',width:'100%'}}/>
                    {this.switchContent(paymentSelection)}     
            </div>
        )

}
}

export default CommentForm;