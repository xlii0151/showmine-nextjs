import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";


export default (props) => {
  const aList=props.cherries.cherries.map((cherry)=>{
    return (
      <div key={cherry.id} className="cherryPic">
        <img src={cherry.image} style={{maxHeight: '100%',
      maxWidth: '100%',
      width: 'auto',
      height: 'auto',
      position: 'absolute',
      top: '0',
      bottom: '0',
      left:'0',
      right: '0',
      margin: 'auto', }} />
      </div>
    );
  });
  return (
  <Carousel showThumbs={false} showStatus={false} showArrows={false} dynamicHeight >
    {aList}
  </Carousel>
  );
};
