import FooterComponent from './footerComponent';

const Layout = props => (
    <div>
      {props.children}
      <FooterComponent />
    </div>
  );
  
  export default Layout; 