import React, {Component} from 'react';
import {Navbar,  Nav, NavItem,
Button, ModalHeader,ModalBody,
Form,FormGroup,Input,Label,col} from 'reactstrap';
import Modal from 'react-responsive-modal';
import Lang from "../lang/Lang";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt, faChargingStation } from '@fortawesome/free-solid-svg-icons';

class Header extends Component {
    constructor(props){
        super(props);
        this.state={
            open:false,
            searchContent:'',
            region:Lang.translate("PLACE_0"),
            languageType:true
        };
    
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleLanguage = this.handleLanguage.bind(this);

    if (typeof window !== 'undefined'){
        if (window.performance) {
            if (performance.navigation.type == 1) {
                localStorage.setItem("language","cn")
            } 
          }
        }
    }
    

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        if (target.value=='null'){
            console.log('image upload failed')
        }
        this.setState({
        [name]: value
        });
    }
    handleSearch(event) {
        console.log('Current State is: ' + JSON.stringify(this.state.searchContent)+JSON.stringify(this.state.region));
        alert('Current State is: ' + JSON.stringify(this.state.searchContent)+JSON.stringify(this.state.region));
        event.preventDefault();
        this.props.resetFeedbackForm();
    }

    handleLanguage(lang) {
        localStorage.setItem("language",lang)
        this.props.sendLanguage(lang)
        this.setState({languageType:!this.state.languageType})
        this.onCloseModal()
    }
    onOpenModal = () => {
        this.setState({ open: true });
      };
    
    onCloseModal = () => {
        this.setState({ open: false });
      };
    render(){
        const { open } = this.state
        const language=this.state.languageType? <div style={{transform:'translateX(-10px)'}}><img className="flag-img-size" src={"flag-china.jpg"} /><span style={{position:'absolute',transform:'translate(10px,10px)'}}>简体中文</span></div>:<div><img className="flag-img-size" src={"flag-australia.jpg"} /><span style={{position:'absolute',transform:'translate(10px,10px)'}}>English</span></div>
        return(
            <div className='header'>
                <Navbar >
                    <div className="headerContainer">
                        <div style={{flex:'0 0 18%'}} className="showMineLogo">
                            <div style={{marginRight:'10px',float:'left'}}><img src='logo.png' height="50" width="50" alt='Showmine' /></div>
                            
                            <div style={{overflow:'auto',marginTop:'8px'}}>
                            <div style={{fontWeight:'700',fontSize:'18px'}}>ShowMine</div>
                            <div style={{fontWeight:'700',fontSize:'12px'}}>秀脉-实惠看得见</div>
                            </div>
                        </div>
                        <Nav navbar className="BigSearchBox">
                            <FormGroup row>
                                <Input style={styles.searchbox} className="searchbox" type="text" id="searchContent" name="searchContent" value={this.state.searchContent} 
                                onChange={this.handleInputChange} placeholder={Lang.translate("SEARCHING")} />
                                <FontAwesomeIcon icon={faMapMarkerAlt} size="2x" style={styles.searchIcon}/>
                                <Input style={styles.areabox} type="select" name="region"
                                            value={this.state.region}
                                            onChange={this.handleInputChange}>
                                        <option>{Lang.translate("PLACE_0")}</option>
                                        <option>{Lang.translate("PLACE_1")}</option>
                                        <option>{Lang.translate("PLACE_2")}</option>
                                        <option>{Lang.translate("PLACE_3")}</option>
                                </Input>
                                <button style={styles.searchbutton} onClick={this.handleSearch}><i className="fa fa-search fa-2x"></i></button>
                            </FormGroup>    
                        </Nav>
                        <Nav className="ml-auto" navbar>
                            <FormGroup row>
                                        <Button style={styles.langbutton} onClick={this.onOpenModal}>
                                            {language}
                                        </Button>
                            </FormGroup>
                        </Nav>
                       
                    </div>
                </Navbar>
                
                <Modal open={open} onClose={this.onCloseModal} center modalId="ModalCss" showCloseIcon={false}>
                    <div style={{textAlign:'center',fontSize:'16px'}}>请选择您的语言 (Please select your language)</div>
                    <ModalBody>
                        <Form>
                        <div style={{alignItems:'center',justifyContent: 'center',display:'flex',margin:'auto'}}>
                        
                        <Button value="submit" style={styles.langbutton1} onClick={()=>this.handleLanguage('cn')}><img className="flag-img-size" src={"flag-china.jpg"} />中文</Button>
                        
                        <Button value="submit" style={styles.langbutton1} onClick={()=>this.handleLanguage('en')}><img className="flag-img-size" src={"flag-australia.jpg"} />English</Button>
                        
                        </div>
                        </Form>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
    
}
const styles={
    searchbox:{
        borderRadius: '6px',
        padding:'15px 15px',
        fontSize:'16px',
        border:'none',
        marginRight:'7px',
        transform:'translateY(-2px)',
    },
    areabox:{
        borderRadius: '6px',
        fontSize:'16px',
        border:'none',
        paddingTop:'15px',
        paddingBottom:'15px',
        paddingLeft:'40px',
        height:'48px',
    },

    searchbutton:{
        padding:'13px',
        borderRadius:'8px',
        border:'1.5px solid white',
        background:'transparent',
        color:'white',
        marginLeft:'7px',
        fontSize:'10px',
        width:'52px'
    },
    searchIcon:{
        position:'absolute',
        transform:'translate(10px,10px)',
        color:'lightgrey'
    },
    langbutton:{
        padding:'12px',
        border:'none',
        background:'transparent',
        color:'black',
        marginLeft:'7px',
        fontSize:'16px',
        fontWeight:'600',
        width:'200px'
    },
    langbutton1:{
        padding:'12px',
        border:'none',
        background:'transparent',
        color:'black',
        marginLeft:'7px',
        fontSize:'16px',
        fontWeight:'600',
        width:'200px',
        flex:'0 0 50%'
    }
}
export default Header;
 