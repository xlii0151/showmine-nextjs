import React, {Component} from 'react';
import {Navbar, NavbarBrand, Nav, NavItem,NavbarToggler,Collapse,
Button, ModalHeader,ModalBody,
Form,FormGroup,Input,Label} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImage } from '@fortawesome/free-solid-svg-icons';
import { faVideo } from '@fortawesome/free-solid-svg-icons'
import { faBuilding } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faComments } from '@fortawesome/free-solid-svg-icons'
import Modal from 'react-responsive-modal';
import Link from 'next/link';
import Head from 'next/head';
import Language from '../lang/Lang';


  
  
function FooterComponent({ children }) {
    return (
      <div>
        <div className="footerStyle">{children}</div>
      </div>
    );
  }
class Footer extends Component {
    constructor(props){
        super(props);
        this.state={
            isNavOpen:false,
            open:false,
            textarea:'',
            images:[],
            videos:[]
        };
        
        this.handleLogin = this.handleLogin.bind(this);
    }
    onOpenModal = () => {
        this.setState({ open: true });
      };
    
    onCloseModal = () => {
        this.setState({ open: false });
      };
    handleLogin(event) {
        console.log('Current State is: ' + JSON.stringify(this.state));
        alert('Current State is: ' + JSON.stringify(this.state));
        event.preventDefault();
        this.props.resetFeedbackForm();

    }

    onImageChange = (event) => {
        
        if (event.target.files && event.target.files[0]) {
            var files=event.target.files
            for (var i = 0, f; f = files[i]; i++) {
            if (f.type.match('image.*')) {
                
            
          let reader = new FileReader();
          reader.onload = (e) => {
            this.setState(
                prevState=>({
                    images:prevState.images.concat(<img style={{float:  'left',
                        maxWidth:  '100px',
                        maxHeight:'100px',
                       
                    }} src={e.target.result} alt="dummy"  />)
                }))
            
            
          };
          
            reader.readAsDataURL(event.target.files[i])
        }
        else if (f.type.match('video.*')){
            let reader = new FileReader();
          reader.onload = (e) => {
            this.setState(
                prevState=>({
                    videos:prevState.videos.concat(<video style={{float:  'left',
                        maxWidth:  '100px',
                        maxHeight:'100px',
                       
                    }} src={e.target.result} alt="dummy"  controls fullScreen={true}/>)
                }))
            
            
          };
          
            reader.readAsDataURL(event.target.files[i])
        }
        }
        }
      }


    removeUpload(index){       
              
        this.setState({
            images: this.state.images.filter((_, i) => i !== index)
          });
    }
    removeUpload1(index){       
              
        this.setState({
            videos: this.state.videos.filter((_, i) => i !== index)
          });
    }
    render(){
        const { open } = this.state;
        return(
            
            <div>
                <FooterComponent>

                <nav>
                    <Link href='/'>
                        <div style={{float:'left',width:'60px',padding:'5px',cursor:'pointer'}}>
                            <a style={{fontSize:'22px'}}><FontAwesomeIcon icon={faBuilding} size="lg"/></a><div style={{fontSize:'12px',padding:'2px'}}>{Language.translate("FOOTER.HOME")}</div>
                        </div>
                    </Link>
                    

                    <div style={{float:'left',width:'80px',padding:'5px',cursor:'pointer',border:'none',color:'inherit',backgroundColor:'transparent',padding:'5px'}} onClick={this.onOpenModal}>        
                        <a style={{fontSize:'13px'}}><FontAwesomeIcon icon={faComments} size="2x"/></a><div style={{fontSize:'12px',padding:'2px'}}>{Language.translate("FOOTER.CONTACT")}</div>
                    </div>
                    <Link href='/test'>
                        <div style={{overflow: 'auto', backgroundColor:'#ffbc21',border:'none',padding:'5px',cursor:'pointer'}}>               
                            <a style={{fontSize:'22px'}}><FontAwesomeIcon icon={faPlus} size="lg" style={{color:'white'}}/></a> <div style={{fontSize:'12px',color:'white'}}>{Language.translate("FOOTER.BUY")}</div>               
                        </div>
                    </Link>       
                </nav>    
                {/* modal must combine with isOpen:true or false */}
                <Modal open={open} onClose={this.onCloseModal} center modalId="ModalCss" showCloseIcon={false}>
                     
                     <ModalBody>
                         <Form onSubmit={this.handleLogin}>
                             <FormGroup row style={styles.rows}>
                                 <Input type="textarea" id="textarea" name="textarea" innerRef={(input)=>this.textarea=input} style={{border:'1px solid grey', fontSize:'17px',width:'95%',resize: 'none',marginTop:'10px',}} placeholder="想问点什么？"
                                    rows="6" />
                             </FormGroup>
                             <FormGroup>
                                {this.state.images.map((image,index) => {
                                    return <div key={index} style={{height:'95px', width:'95px',verticalAlign:'middle',alignItems:'center',justifyContent:'center',border: '1px solid #ddd',display:"inline-grid",float:'left',margin:'6px'}}><button type="button" onClick={() => this.removeUpload(index)} className="remove-image" style={{transform:'translate(80px,-8px)'}}>X</button>{image}</div>;
                                })}
                                <div><input type="file" accept="image/*" multiple onChange={this.onImageChange} className="custom-file-input" />  
                                <FontAwesomeIcon icon={faImage} style={{position:'absolute',transform:'translate(-78px,23px)',pointerEvents:'none'}} size="3x" />
                                <span style={{ position: 'absolute', transform: 'translate(-85px,70px)',width:'100px'}}>{Language.translate("COMMENT.ADDPHOTO")}</span>
                                </div>
                                <br />
                                {this.state.videos.map((video,index) => {
                                    return <div key={index} style={{height:'100px', width:'100px',verticalAlign:'middle',alignItems:'center',justifyContent:'center',border: '1px solid #ddd',display:"inline-grid",float:'left',margin:'6px'}}><button type="button" onClick={() => this.removeUpload1(index)} className="remove-image" style={{transform:'translate(80px,-8px)'}}>X</button>{video}</div>;
                                })}
                                <div><input type="file" accept="video/*" multiple onChange={this.onImageChange} className="custom-file-input1" />  
                                <FontAwesomeIcon icon={faVideo} style={{position:'absolute', transform:'translate(-78px,23px)',pointerEvents:'none'}} size="3x" />
                                <span style={{ position: 'absolute', transform: 'translate(-85px,70px)',width:'100px'}}>{Language.translate("COMMENT.ADDVIDEO")}</span>
                                </div>
                            </FormGroup>
                            <br />
                             <Button onClick={this.onCloseModal} style={{borderRadius:'5px',padding:'10px 17px', backgroundColor:'#DADADA',border:'none',float:'left'}}>取消</Button>
                             <Button onClick={this.onCloseModal} style={{borderRadius:'5px',padding:'10px 17px', backgroundColor:'rgb(39, 135, 214)', transform:'translateX(100%)',border:'none',color:'white'}}>私信</Button>
                             <Button type="submit" value="submit" style={{borderRadius:'5px',padding:'10px 17px', backgroundColor:'#CB3D31',border:'none',color:'white',float:'right'}}>公开</Button>
                         </Form>
                     </ModalBody>
                 </Modal>
                </FooterComponent>
           </div>
         );
        
    
}
}
const styles={

    rows:{
        
        fontSize:'16px'
    },

}
export default Footer;