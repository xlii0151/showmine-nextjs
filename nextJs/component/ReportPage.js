import React,{Component} from 'react';
import {faExclamationTriangle} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {Button, ModalHeader,ModalBody,
    Form,FormGroup,Input,Label,Col} from 'reactstrap'; 
import Modal from 'react-responsive-modal';
import Lang from "../lang/Lang";
import Language from '../lang/Lang';

const triggerButton=<div style={{display:'flex', justifyContent:'center',alignItems:'center',fontSize:'10px',color:'grey'}} ><FontAwesomeIcon icon={faExclamationTriangle} size='xs'/>&ensp;举报</div>
class ReportPage extends Component{
    constructor(props){
        super(props);
        this.state={
            open:false,
            message:'',
            mobile:'',
            email:''
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
    }

    handleSubmit(event) {
        console.log('Current State is: ' + JSON.stringify(this.state));
        alert('Current State is: ' + JSON.stringify(this.state));
        event.preventDefault();
        this.props.resetFeedbackForm();
    }
    onOpenModal = () => {
        this.setState({ open: true });
      };
    
    onCloseModal = () => {
        this.setState({ open: false });
      };
    
    render(){
        const SectionDivider=<div className='sectionDivider'><div style={{height: '15px',
        border: '0',boxShadow: 'inset 0px 3px 3px #C3C3C3',width:'100%',transform:'translate(0px,8px)',
        backgroundColor:'#F7F7F7'}}></div><br /></div>
        const { open } = this.state;
        return (
            <div className="DetailContainer" id="reportLaptop">
                {SectionDivider}
                <div style={{display:'flex', justifyContent:'center',alignItems:'center',fontSize:'10px',color:'grey',height:'20px'}} onClick={this.onOpenModal}><FontAwesomeIcon icon={faExclamationTriangle} size='xs'/>&ensp;{Lang.translate("REPORT")}</div>
                <Modal open={open} onClose={this.onCloseModal} center modalId="ModalCss" showCloseIcon={false}	>
            
                    <b style={{margin:'10px 0px'}}>{Language.translate("REPORT.ITEM")}</b>
                    <ModalBody>
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup row style={styles.rows}>
                                <Label htmlFor="email" style={styles.text}>{Language.translate("REPORT.EMAIL")}</Label>
                                <Input style={styles.box} type="text" id="email" name="email" value={this.state.email} onChange={this.handleInputChange} placeholder={Language.translate("REPORT.EMAIL1")} />
                            </FormGroup>
                            <FormGroup row style={styles.rows}>
                                <Label htmlFor="mobile" style={styles.text}>{Language.translate("REPORT.PHONE")}</Label>
                                <Input style={styles.box} type="mobile" id="mobile" name="mobile" value={this.state.mobile} onChange={this.handleInputChange} placeholder={Language.translate("REPORT.PHONE1")}/>
                            </FormGroup>
                            <FormGroup row style={styles.rows}>
                                <Label htmlFor="message"></Label>{Language.translate("REPORT.REASON")}
                                
                                    <Col md={10}>
                                        <Input style={{fontSize:'14px',padding:'10px',width:"90%"}} type="textarea" id="message" name="message"
                                            rows="3"
                                            value={this.state.message}
                                            onChange={this.handleInputChange} placeholder={Language.translate("REPORT.REASON1")}></Input>
                                    </Col> 
                            </FormGroup>
        <Button  onClick={this.onCloseModal} style={{borderRadius:'5px',padding:'10px 17px', backgroundColor:'#DADADA',border:'none',float:'left'}}>{Language.translate("REPORT.CANCEL")}</Button>
        <Button type="submit" value="submit" style={{borderRadius:'5px',padding:'10px 17px', backgroundColor:'#CB3D31',border:'none',color:'white',float:'right'}}>{Language.translate("COMMENT.SUBMIT")}</Button>
                        </Form>
                    </ModalBody>
                        
            </Modal>
            {SectionDivider}
           </div>
            
        )
    }
}

const styles={
    box:{
        borderRadius: '2px',
        width:'90%',
        padding:'5px',
        fontSize:'16px'
    },
    rows:{
        margin:'10px 0px',
        fontSize:'16px'
    },
    text:{
        margin:'0px 10px 0px 0px'
    }
}
export default ReportPage;
