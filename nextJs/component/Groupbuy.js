import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Button, Form, FormGroup, Label, Input, Col } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImage } from '@fortawesome/free-solid-svg-icons';
import { faSortDown } from '@fortawesome/free-solid-svg-icons';
import { faSortUp } from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import StarRatings from 'react-star-ratings';
import Lang from "../lang/Lang";
class Groupbuy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: '',
            images: [],
            rating: 3,
            scrolled: true,
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggleBar = this.toggleBar.bind(this)
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        if (target.value == 'null') {
            console.log('image upload failed')
        }
        this.setState({
            [name]: value
        });
    }
    removeUpload(index) {
        this.setState({
            images: this.state.images.filter((_, i) => i !== index)
        });
    }
    handleSubmit(event) {
        console.log('Current State is: ' + JSON.stringify(this.state));
        alert('Current State is: ' + JSON.stringify(this.state));
        event.preventDefault();
        this.props.resetFeedbackForm();
    }
    toggleBar() {

        this.setState(prevState => ({ scrolled: !prevState.scrolled }))

    }
    handleRatingChange = (newRating, name) => {
        this.setState({ rating: newRating });
    }
    onImageChange = (event) => {

        if (event.target.files && event.target.files[0]) {
            var files = event.target.files
            for (var i = 0, f; f = files[i]; i++) {
                if (!f.type.match('image.*')) {
                    continue;
                }
                let reader = new FileReader();
                reader.onload = (e) => {
                    this.setState(
                        prevState => ({
                            images: prevState.images.concat(<img style={{
                                float: 'left',
                                maxWidth: '100px',
                                maxHeight: '100px',

                            }} src={e.target.result} alt="dummy" />)
                        }))


                };

                reader.readAsDataURL(event.target.files[i])

            }
        }
    }
    render() {
        const scrollBg = this.state.scrolled ? faSortDown : faSortUp;

        return (

            <div className="container">

                <div className="row row-content" >

                    <div style={{ height: '30px', backgroundColor: '#F6F6F5', borderRadius: '8px 8px 0px 0px', padding: '13px 0px 0px 15px', borderBottom: '1px solid lightgrey' }}>{Lang.translate("COMMENT.POST")}<div style={{ float: 'right', transform: 'translateX(-10px)' }}><button style={{ border: 'none', backgroundColor: 'transparent' }} onClick={this.toggleBar}><FontAwesomeIcon icon={scrollBg} size='1x' /></button></div></div>
                    {this.state.scrolled &&
                        <Form onSubmit={this.handleSubmit} >

                            <FormGroup row>
                                <Col md={10}>
                                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                        <Input id="myInput" type="textarea" name="message" style={{ border: 'none!important', fontSize: '17px', border: 'none', width: '95%', resize: 'none', marginTop: '10px' }} placeholder={Lang.translate("COMMENT.PLACEHOLDER")}
                                            rows="3"
                                            value={this.state.message}
                                            onChange={this.handleInputChange}></Input></div>
                                </Col>
                            </FormGroup>
                            <FormGroup row >
                                <Col md={10} >
                                    <div className="UploadLaptop">
                                        {this.state.images.map((image, index) => {
                                            return <div key={index} style={{
                                                height: '100px', width: '100px', verticalAlign: 'middle', alignItems: 'center', justifyContent: 'center', border: '1px solid #ddd',
                                                margin: '7px', display: "inline-grid", float: 'left'
                                            }}><button type="button" onClick={() => this.removeUpload(index)} className="remove-image">X</button>{image}</div>;
                                        })}
                                    </div>
                                    <div className="UploadLaptop"><input type="file" accept="image/*" multiple onChange={this.onImageChange} className="custom-file-input" style={{ margin: '7px' }} />

                                        <FontAwesomeIcon icon={faImage} style={{ position: 'absolute', transform: 'translate(-85px,30px)', pointerEvents: 'none' }} size="3x" />
                                        <span style={{ position: 'absolute', transform: 'translate(-95px,80px)',width:'100px' }}>{Lang.translate("COMMENT.ADDPHOTO")}</span>
                                    </div>
                                    

                                </Col>

                            </FormGroup>

                            <FormGroup row>
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                    {Lang.translate("COMMENT.COMPANYRATE")}&emsp;<StarRatings
                                        rating={this.state.rating}
                                        starRatedColor="#fcdb03"
                                        starHoverColor="#fcdb03"
                                        changeRating={this.handleRatingChange}
                                        numberOfStars={5}
                                        starDimension='30px'
                                        starSpacing="9px"
                                        name='rating'
                                    /></div>
                            </FormGroup>

                            <FormGroup row>
                                <Col md={{ size: 10, offset: 2 }}>
                                    <div style={{ textAlign: 'center', marginLeft: 'auto', marginRight: 'auto', marginTop: '15px', marginBottom: '15px' }}>
                                        <Button type="submit" style={{ padding: '10px 30px', borderRadius: '6px', backgroundColor: '#3480fa', color: 'white', border: 'none' }}>
                                            {Lang.translate("COMMENT.SUBMIT")}
                                </Button>
                                    </div>
                                </Col>
                            </FormGroup>

                        </Form>
                    }

                </div>
            </div>

        );
    }

}


export default Groupbuy;