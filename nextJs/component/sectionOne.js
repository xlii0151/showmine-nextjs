import React from "react";
import Lang from "../lang/Lang";

const SectionDivider=<div className='sectionDivider'><div style={{height: '15px',
border: '0',
boxShadow: 'inset 0px 3px 3px #C3C3C3',
width:'100%',
transform:'translate(0px,8px)',
backgroundColor:'#F7F7F7'}}></div><br /></div>
export default (props)=>{  
    return (
        <div>
        {SectionDivider}
        <div className="DetailContainer">
            <div className="CardDetail">
                <div className="PicDetail">
                    <div><img src={'cherry1.jpg'} alt='cherry' className="PicWidth"/></div>
                </div>
                <div className="PicDetailSub">
                    <div>澳洲本地-顶级杨梅</div>
                    <div style={{fontSize:'18px',padding:'8px 0px',color:'#990000'}}>$58</div>
                    <div style={{fontSize:'11px'}}>{Lang.translate("USER.PARTICIPANT")}0<span style={{display:'inline-block'}}>&emsp;{Lang.translate("USER.CHECKEDIN")}0</span></div>
                </div>
            </div>
        </div>
        <div className="DetailContainer">
        {SectionDivider}         
        <div style={{fontSize:'12px',margin:'0px 5px'}} className='Participate'>{Lang.translate("USER.PARTICIPANT")}0&emsp;{Lang.translate("USER.CHECKEDIN")}0&emsp;{Lang.translate("USER.USERS")}0</div>
        </div>
        </div>

    )
}