import React, {Component} from 'react'
import Lang from "../lang/Lang";
export default class CountDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      day: 0,
      hour: 0,
      minute: 0,
      second: 0
    }
  }
  componentDidMount() {
    if(this.props.endTime){
      let endTime = this.props.endTime.replace(/-/g, "/");
      this.countFun(endTime);
    }
  }
  //组件卸载取消倒计时
  componentWillUnmount(){
    clearInterval(this.timer);
  }
  
  countFun = (time) => {
    let end_time = new Date(time).getTime(),
    sys_second = (end_time - new Date().getTime());
    this.timer = setInterval(() => {
    //防止倒计时出现负数
      if (sys_second > 1000) {
        sys_second -= 1000;
        let day = Math.floor((sys_second / 1000 / 3600) / 24);
        let hour = Math.floor((sys_second / 1000 / 3600) % 24);
        let minute = Math.floor((sys_second / 1000 / 60) % 60);
        let second = Math.floor(sys_second / 1000 % 60);
        this.setState({
          day:day,
          hour:hour < 10 ? "0" + hour : hour,
          minute:minute < 10 ? "0" + minute : minute,
          second:second < 10 ? "0" + second : second
        })
      } else {
        clearInterval(this.timer);
        //倒计时结束时触发父组件的方法
        // this.props.timeEnd();
      }
    }, 1000);
  }
  
  render() {
    console.log('successful');
    return (
      <div style={{margin:'0px 5px'}}>
      <span style={{fontSize:"14px",float:'left'}}>{Lang.translate("COUNTDOWN")}{this.state.day}{Lang.translate("DAY")} {this.state.hour}{Lang.translate("HOURS")}{this.state.minute}{Lang.translate("MINUTES")}{this.state.second}{Lang.translate("SECONDS")}
      </span>
      <span style={{float:'right',fontSize:'14px'}}>{Lang.translate("VISIT")}</span><br />
      <div style={{fontSize:'14px'}}>{Lang.translate("AREA")}</div>
      <span style={{float:'right',fontSize:'14px'}}>{this.props.visit&&this.props.visit.value+1}</span>
      
      </div>
      
    )
  }
}