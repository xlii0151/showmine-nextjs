import React from "react";
import {faAngleRight} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import Language from "../lang/Lang";
import Footer from '../component/footerComponent';
const SectionDivider=<div className='sectionDivider'><div style={{height: '15px',
border: '0',boxShadow: 'inset 0px 3px 3px #C3C3C3',width:'100%',transform:'translate(0px,8px)',
backgroundColor:'#F7F7F7'}}></div><br /></div>

export default (props)=>{  
    return (
        <div className="DetailContainer">
        <Link href='/'>
        <div>
        <div style={{display:'flex', justifyContent:'center',alignItems:'center',fontSize:'14px',height:'20px'}} >{Language.translate("TRADE")}<span style={{fontSize:'12px',color:'grey'}}>&ensp;{Language.translate("TRADE.USERAGREE")}</span></div>
          <FontAwesomeIcon icon={faAngleRight} size="1x" style={{float:'right',color:'darkblue',transform:'translateY(-20px)'}} />
        </div>
        </Link>

        {SectionDivider}
        <Link href='/'>
          <button style={styles.buttons}>{Language.translate("LOGIN")}</button>
        </Link>
        <div className='sectionDivider'><div style={{height: '15px',
border: '0',boxShadow: 'inset 0px 3px 3px #C3C3C3',width:'100%',transform:'translate(0px,-10px)',
backgroundColor:'#F7F7F7'}}></div><br /></div>
        <Footer />
        <div className="groupbuyMargin">
          <div className="groupBuyDetail">
            <span>{Language.translate("LATESTGROUPBUY")}</span>
            <Link href='/'>
            <span style={{float:'right',color:'#2787d6'}}>{Language.translate("CHECKMORE")}</span>
            </Link>
          </div>
          
          <div><img src={'cherry1.jpg'} width='15%' alt="cherry"/></div>
        </div>
        {SectionDivider}
        </div>
    )
}
const styles={
    buttons:{
        borderRadius: '8px',
        width: '100%',
        height:'40px',
        fontSize: '16px',
        backgroundColor:'#2E77B7',
        border:'none',
        textAlign:'center',
        justifyContent:'center',
        color:'white',
        transform:'translateY(-10px)'
    }
  }
  