import React from "react";
import { css } from "emotion";

export const NiceGrid = ({
  children,
  maxItemsPerLine,
  itemMinWidth,
  itemAspectRatio,
  gutterSize
}) => {
  const gridClass = css`
    display: flex;
    flex-wrap: wrap;
    align-items: stretch;
    ${gutterSize && css`margin: calc(${gutterSize} * -0.5);`};
  `;
  const itemClass = css`
    flex: 1 0 ${(1 / maxItemsPerLine) * 100}%;
    min-width: ${itemMinWidth};
    ${itemAspectRatio && css`position: relative;`};
    ${!itemAspectRatio &&
      gutterSize &&
      css`padding: calc(${gutterSize} * 0.5);`};
    
  `;
  const squareFillerClass = css`
    padding-top: ${(1 / itemAspectRatio) * 100}%;
  `;
  const squareItemWrapperClass = css`
    position: absolute;
    width: 100%;
    height: 100%;
    top:0;
    left:0;
    border-bottom: 1px solid #dfdfdf;
    border-right: 1px solid #dfdfdf;
    
    ${gutterSize && css`padding: calc(${gutterSize} * 0.5);`};
  `;

  return (
    <div className={gridClass}>
      {React.Children.map(children, child => (
        <div className={itemClass}>
          {itemAspectRatio && <div className={squareFillerClass} />}
          {itemAspectRatio ? (
            <div className={squareItemWrapperClass}>{child}</div>
          ) : (
            child
          )}
        </div>
      ))}
      {(children.length>maxItemsPerLine)&&Array.from(Array((maxItemsPerLine % children.length) - 1)).map(
        (arg, index) => <div className={itemClass} />
      )}
    </div>
  );
};

export default NiceGrid;
