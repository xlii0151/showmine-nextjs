import React from 'react';
import {Provider} from 'react-redux';
import {ConfigureStore} from '../redux/store';
const store = ConfigureStore();
const wrapperRedux = (Page) => {
    return(
        class PageWrapper extends React.Component{
            
            render(){
                return(
                    <Provider store={store}>
                        <Page />
                    </Provider>
                )
            }
        }
    )
}
export default wrapperRedux;