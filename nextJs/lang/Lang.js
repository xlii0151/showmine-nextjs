import React, {Component} from 'react';
import CN from './cn';
import EN from './en';

class Lang {
    translate = (string) => {
        let language = 'cn'
        if (typeof window !== 'undefined') {
            language = window.localStorage.getItem('language');
            
        }
        
        if (language == 'cn' && CN[string]) {
                return CN[string];
        }
        else if (language == 'en' && EN[string]) {
                return EN[string];
        }
        
            
           
        
        // systemLang=localStorage.getItem("language")
        // if (window.navigator && window.navigator.language.includes('en')) {
        //     systemLang = 'en'
        // }
        // if (window.navigator && window.navigator.language.includes('zh')) {
        //     systemLang = 'cn'
        // }
        // console.log(localStorage&&localStorage.getItem("language"))
    }
}
 
const Language = new Lang();
export default Language;
