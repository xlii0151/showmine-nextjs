import React,{useEffect,useState,useRef} from "react";
import Header from '../component/headerComponent';
import Footer from '../component/footerComponent';
import Carousel from '../component/carousel';
import BigBox from '../component/bigBox';
import CountDown from '../component/countDown';
import WrapperRedux from '../component/WrapperRedux';
import {connect} from 'react-redux';
import ReactGa from 'react-ga';
import {fetchCount,postCount} from '../redux/actionCreator';
import Hidden from '../component/hiddenMore';
import CommentForm from '../component/commentComponent';
import SectionOne from '../component/sectionOne';
import {actions} from 'react-redux-form';
import ReportPage from '../component/ReportPage';
import SectionTwo from '../component/sectionTwo';
import './app.css';
import Head from 'next/head';
import {Navbar,  Nav, NavItem,
  Button, ModalHeader,ModalBody,
  Form,FormGroup,Input,Label,col} from 'reactstrap';
import Modal from 'react-responsive-modal';


const mapDispatchToProps=dispatch=>({
  resetFeedbackForm: ()=>{dispatch(actions.reset('feeedback'))},
  // fetchCount:()=>{dispatch(fetchCount())},
  // postCount:(访问次数)=>{dispatch(postCount(访问次数))}
});



function mainPage(props){
  // const visit=props.count.count.filter((count)=>count.featured)[0]
  // const myvisit = useRef();
  // myvisit.current = visit;
  // const fetchShit = () => {
  //   props.fetchCount();
  // }
  // const fetchShit1 = (访问次数) => {
  //   props.postCount(访问次数)
    
  // }
  // useEffect(()=>{
  //   // ReactGa.initialize('UA-156910288-1');
  //   // ReactGa.pageview(window.location.pathname);
  //   fetchShit();
  //   return function cleanup() {
  //     fetchShit1( myvisit.current&& myvisit.current.value+1);
  //   };
  // },[])


    
    
  
  const Slideshow = () => {
    const [language, setLanguage] = useState('');
    const getData=(val)=>{
        setLanguage(val)      
    }
    return (
      <div className="contentWrapper">
        <Head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            crossOrigin="anonymous"/>
        </Head>
        <Header resetFeedbackForm={props.resetFeedbackForm} sendLanguage={getData}/>

        <div className="bodyContainer">
          <div className='carouselStyle'>
            <Carousel cherries={props.cherries}/>
          </div>
          <div className='productInfoStyle'>
            {/* <CountDown endTime={'2020-08-23 00:00:00'} visit={visit}/> */}
            
            <Hidden/>
            <img src={"piano.png"} style={{width: '95%',display:'flex',
            textAlign:'center',
            justifyContent:'center',margin:"auto",transform:"translateY(25px)"}} />
            <br />
          </div>
        </div>
        <SectionOne />
        
        <CommentForm resetFeedbackForm={props.resetFeedbackForm}/>   

        <ReportPage resetFeedbackForm={props.resetFeedbackForm}/>
        <SectionTwo />
        <BigBox cherries={props.cherries}/>
        
        <br /><br /><br /><br /><br /><br /><br /><br />
        <div className='footer'>
        <Footer resetFeedbackForm={props.resetFeedbackForm} />
        </div>
      </div>
    )
  }
  return (
    <Slideshow />
  )
}


export default WrapperRedux(connect(state=>state,mapDispatchToProps)(mainPage));