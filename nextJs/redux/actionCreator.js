import * as ActionTypes from './actionType';
import {baseUrl} from '../shared/baseUrl';

export const fetchCount = () =>(dispatch)=>{
  const fetch = require("node-fetch");
    dispatch(countLoading(true));
    return fetch(baseUrl+'count')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
        .then(response=>response.json())
        .then(count=>dispatch(addCount(count)))
        .catch(error=>dispatch(countFailed(error.message)));
    }

export const countFailed=(errmess)=>({
    type:ActionTypes.COUNT_FAILED,
    payload:errmess
});

export const addCount=(count)=>({
    type:ActionTypes.ADD_COUNT,
    payload: count
});

export const countLoading=()=>({
    type:ActionTypes.COUNT_LOADING
});

export const postCount1=(newCount)=>({
  type:ActionTypes.POST_COUNT1,
  payload:newCount
})
export const postCount = (number) =>(dispatch)=> {
  const fetch = require("node-fetch");
  const newCount = {
    "id": 0,
    "value": number,
    "featured": true
 
}; 
  dispatch(countLoading(true));
  return fetch(baseUrl + 'count/0', {
      method: "PUT",
      body: JSON.stringify(newCount),
      headers: {
        'Accept': 'application/json',
      "Content-type": "application/json",
      
    },
      credentials: "same-origin"
  })
  .then(response => {
    if (response.ok) {
      return response;
    } else {
      var error = new Error('Error ' + response.status + ': ' + response.statusText);
      error.response = response;
      throw error;
    }
  },
  error => {
        throw error;
  })
  
.then(response => response.json())
.then(response => dispatch(postCount1(response)))
.catch(error =>  { console.log('put count', error.message); alert('Your visit could not be updated\nError: '+error.message); });
  }


export default fetchCount;