import * as ActionTypes from '../actionType'

export const Count = (state = {
    count:[],
    isLoading:true,
    errMess:null
}, action) => {
    switch (action.type){
        case ActionTypes.ADD_COUNT:
            console.log(action.payload)
            console.log("add了")
            return {...state, count: action.payload, isLoading: false,errMess:null};

        case ActionTypes.COUNT_LOADING:
            return {...state, isLoading: true, errMess: null}

        case ActionTypes.COUNT_FAILED:
            return { ...state, isLoading: false,errMess: action.payload};
        case ActionTypes.POST_COUNT1:
            console.log(action.payload)
            console.log("post了")
            var counts=action.payload
            return { ...state, count: state.count.concat(counts)};
        default:
            return state;
    
    }
};