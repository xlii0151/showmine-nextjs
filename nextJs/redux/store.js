import {createStore, combineReducers,applyMiddleware} from 'redux';
import {Cherries} from './reducers/cherries';
import {Count} from './reducers/count';
import { InitialFeedback } from './reducers/forms';
import { createForms } from 'react-redux-form';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
/*
    El store es el centro neuralgico de Redux, por aca se entra a redux y los componentes
    entran en interaccion con redux. en pocas palabras este es el patio del modulo ksaen.
*/
export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            cherries:Cherries,
            count:Count,
            ...createForms({
                feedback: InitialFeedback})
        }),
        applyMiddleware(thunk,logger)
    );

    return store;
}